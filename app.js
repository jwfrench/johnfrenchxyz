// Global variables for linter to ignore:

// Import Required Modules
const express = require('express');
const nunjucks = require('nunjucks');
const feather = require('feather-icons');
const app = express();
const fs = require('fs');
// const portfolioData = require('./portfolio.json');

// Global Variables
const port = 3000;

// Get nunjucks going
nunjucks.configure('templates', {
	autoescape: true,
	noCache: true,
	express: app
});

app.get('/', (req, res) => {
	res.render('index.njk', JSON.parse(fs.readFileSync('./portfolio.json')));
});

app.get(/\/pages\/.+/, (req, res) => {
	res.render(req.url.substring(1).replace('.html', '.njk'));
});

// Turn on public folder
app.use(express.static('public', {index: false}));

app.listen(port);
