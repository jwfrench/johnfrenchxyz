// Gallery Setup
//--------------

// Extension Gallery
$('#extensionGallery').on('click', function() {
  $(this).lightGallery({
    dynamic: true,
    dynamicEl: [{
      "src": '../images/screens/extension/imac.png'
    }, {
      "src": '../images/screens/extension/extension-iPhone.png'
    }, {
      "src": '../images/screens/extension/publications.png'
    }, {
      "src": '../images/screens/extension/fmco-1.png'
    }, {
      "src": '../images/screens/extension/fmco-2.png'
    }, {
      "src": '../images/screens/extension/fmco-3.png'
    }, {
      "src": '../images/screens/extension/fmco-4.png'
    }, {
      "src": '../images/screens/extension/extension-menu-demo.gif'
    }, {
      "src": '../images/screens/extension/extension-mobile-menu-demo.gif'
    }, {
      "src": '../images/screens/extension/4h.png'
    }]
  })
});

// Shelf Gallery
$('#shelfGallery').on('click', function() {
  $(this).lightGallery({
    dynamic: true,
    dynamicEl: [{
      "src": '../images/screens/shelf/shelf-1.png'
    }, {
      "src": '../images/screens/shelf/shelf-2.png'
    }, {
      "src": '../images/screens/shelf/shelf-3.png'
    }, {
      "src": '../images/screens/shelf/shelf-4.png'
    }, {
      "src": '../images/screens/shelf/shelf-5.png'
    }, {
      "src": '../images/screens/shelf/shelf-6.png'
    }]
  })
});

// CAES Gallery
$('#caesGallery').on('click', function() {
  $(this).lightGallery({
    dynamic: true,
    dynamicEl: [{
      "src": '../images/screens/caes/caes-iphone.png'
    }, {
      "src": '../images/screens/caes/caes-banner.png'
    }, {
      "src": '../images/screens/caes/caes-components.png'
    }, {
      "src": '../images/screens/caes/caes-calendar.png'
    }, {
      "src": '../images/screens/caes/caes-footer.png'
    }]
  })
});

// Bloodroot Gallery
$('#bloodrootGallery').on('click', function() {
  $(this).lightGallery({
    dynamic: true,
    dynamicEl: [{
      "src": '../images/screens/bloodroot/bloodroot-iphone.png'
    }, {
      "src": '../images/screens/bloodroot/bloodroot-ipad.png'
    }, {
      "src": '../images/screens/bloodroot/bloodroot-detail.png'
    }, {
      "src": '../images/screens/bloodroot/bloodroot-gallery.png'
    }, {
      "src": '../images/screens/bloodroot/bloodroot-gallery-detail.png'
    }]
  })
});

  // Oak House Gallery
  $('#oakHouseGallery').on('click', function() {
    $(this).lightGallery({
      dynamic: true,
      dynamicEl: [{
        "src": '../images/screens/oak-house/oak-house-iphone.png'
      }, {
        "src": '../images/screens/oak-house/oak-house-news.png'
      }, {
        "src": '../images/screens/oak-house/oak-house-music.png'
      }, {
        "src": '../images/screens/oak-house/oak-house-tour.png'
      }, {
        "src": '../images/screens/oak-house/oak-house-home.png'
      }]
    })
  });

  // Oak House Gallery
  $('#subsiteGallery').on('click', function() {
    $(this).lightGallery({
      dynamic: true,
      dynamicEl: [{
        "src": '../images/screens/subsite/1.png'
      }, {
        "src": '../images/screens/subsite/2.png'
      }, {
        "src": '../images/screens/subsite/3.png'
      }, {
        "src": '../images/screens/subsite/4.png'
      }, {
        "src": '../images/screens/subsite/5.png'
      }, {
        "src": '../images/screens/subsite/6.png'
      }, {
        "src": '../images/screens/subsite/7.png'
      }, {
        "src": '../images/screens/subsite/8.png'
      }]
    })
  });
